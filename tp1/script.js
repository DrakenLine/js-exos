document.addEventListener('DOMContentLoaded', () => {

//    Parcourir tous les éléments dont la classe est "item"

// Dès que le texte à l'intérieur correspond à "cible 1"
// Retirer la classe "light-grey" de l'item
// Ajouter la classe "pink" à cet item
// Incrémenter la valeur du compteur de l'élément HTML correspondant à la classe "target" de la cible 1

// Dès que le texte à l'intérieur correspond à "cible 2"
// Retirer la classe "light-grey" de l'item
// Ajouter la classe "red" à cet item
// Incrémenter la valeur du compteur de l'élément HTML correspondant à la classe "target" de la cible 2


   const item = document.getElementsByClassName('item');
   const pink = document.getElementsByClassName('pink');
   const red = document.getElementsByClassName('red');
   const target = document.getElementsByClassName('target');

   for (var i = 0; i < 5; i++) {

	   	if (item[i].lastElementChild.innerHTML == "cible 1") {
	   	item[i].classList.remove('light-grey');
	   	item[i].classList.add('pink');

		   	// console.log(target[0].firstElementChild.innerHTML)

		   	if (target[0].firstElementChild.innerHTML == "cible 1") {
		   		target[0].lastElementChild.innerHTML = parseInt(target[0].lastElementChild.innerHTML) + 1;

		   	}
   	
   		}

	   if(item[i].lastElementChild.innerHTML == "cible 2"){
	   	item[i].classList.remove('light-grey');
	   	item[i].classList.add('red');


			if (target[1].firstElementChild.innerHTML == "cible 2") {
		   		target[1].lastElementChild.innerHTML = parseInt(target[1].lastElementChild.innerHTML) + 1;

		   	}
	   }
   }
   
})